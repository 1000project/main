var b = document.getElementById('name');

$('#applyBtn').click(function () {
    // alert('welcome')
    $('#applyBtn').attr('disabled', 'disabled')
    var name = $('#name').val();
    var username = $('#username').val();
    var post = $('#post').val();
    var phonenumber = $('#phonenumber').val();
    var packagePrice = $('#package').text();
    var price = $('#packagePrice').text();
    $('.loader_tb').css('display', 'block');

    $.ajax(
        // name, username, phone, package, price
        {
            method: "post",
            url: "https://api.outsmarters.net/api/",
            data: {
                name: name,
                username: username,
                phone: phonenumber,
                package: packagePrice,
                price: price,
                post: post
            }
        }
    ).done(response => {
        $('.loader_tb').css('display', 'none');
        console.log(response);
        $.notify("Your Request Recieved", "success");
        setTimeout(() => {
            window.location.replace('/')
        }, 2000);

    }).catch(e => {
        $.notify("Please try again", "error");
        console.log('get Error', e.message)
    })
})
